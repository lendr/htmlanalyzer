package edu.vgtu.analyzer.constants;

public class HighChartsConstants {
    public static final String pieCategory = "\n" +
            " {\n" +
            "    chart: {\n" +
            "        plotBackgroundColor: null,\n" +
            "        plotBorderWidth: null,\n" +
            "        plotShadow: false,\n" +
            "        type: 'pie'\n" +
            "    },\n" +
            "    tooltip: {\n" +
            "        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'\n" +
            "    },\n" +
            "    accessibility: {\n" +
            "        point: {\n" +
            "            valueSuffix: '%'\n" +
            "        }\n" +
            "    },\n" +
            "    plotOptions: {\n" +
            "        pie: {\n" +
            "            allowPointSelect: true,\n" +
            "            cursor: 'pointer',\n" +
            "            dataLabels: {\n" +
            "                enabled: false\n" +
            "            },\n" +
            "            showInLegend: true\n" +
            "        }\n" +
            "    },\n" +
            "    series: [{\n" +
            "        name: 'Категория',\n" +
            "        colorByPoint: true,\n" +
            "        data: []\n" +
            "    }]\n" +
            "})";

    public static final String contentsByTime ="{\n" +
            "    xAxis: {\n" +
            "        categories: ['0-2', '2-4', '4-6', '6-8', '8-10', '10-12', '12-14', '14-16', '16-18', '18-20', '20-22', '22-24']\n" +
            "    },\n" +
            "\n" +
            "    series: [{\n" +
            "        type: 'column',\n" +
            "        colorByPoint: true,\n" +
            "        data: [],\n" +
            "        showInLegend: false\n" +
            "    }]\n" +
            "\n" +
            "}";
}
