package edu.vgtu.analyzer.controllers;

import edu.vgtu.analyzer.dto.ContentsDTO;
import edu.vgtu.analyzer.services.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/graphics")
public class GraphicsController {
    @Autowired
    StatisticService statisticService;

    @GetMapping("/foundContents")
    public List<ContentsDTO> getFoundContentsGraphic(){
        return statisticService.getFoundContents();
    }

    @GetMapping("/categories")
    public String categories(){
        return statisticService.getHighChartsCategoryByContentSize().toString();
    }

    @GetMapping("/contentsByTime")
    public String contentsByTime(){
        return statisticService.getHighChartsContentsByTime().toString();
    }
}
