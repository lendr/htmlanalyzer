package edu.vgtu.analyzer.controllers;

import edu.vgtu.analyzer.services.HtmlParserService;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.util.AttributeImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.tartarus.snowball.ext.PorterStemmer;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/v1")
public class MainController {
    @Autowired
    HtmlParserService parserService;
    public static ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(1);

    @PostMapping("/parse")
    public String parseHtml(@RequestBody List<String> urls) {
        if(urls.size()==0)
            return null;
        if (SCHEDULER.isShutdown()) {
            SCHEDULER = Executors.newScheduledThreadPool(1);
        }
        SCHEDULER.scheduleAtFixedRate(() -> {
                    Date start = new Date();
                    parserService.parse(urls);
                    long time = new Date().getTime() - start.getTime();
                    System.out.println("launch in " + time);
                },
                0,
                20,
                TimeUnit.MINUTES);
        return null;
    }

    @GetMapping("/stop")
    public void stopMonitoring() {
        SCHEDULER.shutdown();
    }

    @GetMapping("/stem")
    public String stem(@RequestParam("text") String text) {
        PorterStemmer stemmer = new PorterStemmer();
        stemmer.setCurrent(text);
        stemmer.stem();
        return stemmer.getCurrent();
    }

    @GetMapping("/lucen")
    public List<String> lucen(@RequestParam("text") String text) throws IOException {
        RussianAnalyzer analyzer = new RussianAnalyzer();
        TokenStream tokenStream = analyzer.tokenStream("", text);
        AttributeImpl attr = tokenStream.getAttributeImplsIterator().next();

        tokenStream.reset();
        List<String> result = new ArrayList<>();
        while (tokenStream.incrementToken()) {
            result.add(attr.toString());
        }

        return result;
    }
}
