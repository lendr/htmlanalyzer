package edu.vgtu.analyzer.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ContentsDTO {
    String url;
    Date foundDate;
    String categories;
}
