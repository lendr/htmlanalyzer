package edu.vgtu.analyzer.entities;

import lombok.Data;
import sun.security.krb5.internal.crypto.Des;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer categoryId;

    String name;

    @Column(length=10485760)
    String terms;

    @ManyToMany(mappedBy = "categoryList")
    List<DestructionContent> destructionContentList;
}
