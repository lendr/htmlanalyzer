package edu.vgtu.analyzer.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class DestructionContent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer contentId;

    @Column(length = 10485760)
    String text;

    String contentHash;

    Date dateFound;

    @ManyToOne
    @JoinColumn
    Resource resource;

    @ManyToMany
    @JoinTable(
            name = "content_categories",
            joinColumns = {@JoinColumn(name = "contentId")},
            inverseJoinColumns = {@JoinColumn(name = "categoryId")}
    )
    List<Category> categoryList;
}
