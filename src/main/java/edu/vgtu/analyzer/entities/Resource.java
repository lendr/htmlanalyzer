package edu.vgtu.analyzer.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Resource {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer resourceId;

    @Column(length = 10485760)
    String url;

    String hash;

    int rank;

    @OneToMany(mappedBy = "resource", cascade = CascadeType.PERSIST)
    List<DestructionContent> destructionContentList;


}
