package edu.vgtu.analyzer.repositories;

import edu.vgtu.analyzer.entities.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriesRepository extends CrudRepository<Category, Integer> {
}
