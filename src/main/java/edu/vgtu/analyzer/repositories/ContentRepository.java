package edu.vgtu.analyzer.repositories;

import edu.vgtu.analyzer.entities.DestructionContent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContentRepository extends CrudRepository<DestructionContent, Integer> {
    DestructionContent findDestructionContentByContentHash(String hash);
    DestructionContent findDestructiveContentByResourceUrl(String url);
//    Long countByCategoryList(List<Category> category);
}
