package edu.vgtu.analyzer.repositories;

import edu.vgtu.analyzer.entities.Resource;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourceRepository extends CrudRepository<Resource, Integer> {
    Resource findByUrl(String url);
}
