package edu.vgtu.analyzer.services;

import edu.vgtu.analyzer.entities.Category;
import edu.vgtu.analyzer.entities.DestructionContent;
import edu.vgtu.analyzer.entities.Resource;
import edu.vgtu.analyzer.repositories.CategoriesRepository;
import edu.vgtu.analyzer.repositories.ContentRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.ru.RussianAnalyzer;
import org.apache.lucene.util.AttributeImpl;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

@Service
public class DestructiveAnalyzer {
    List<Category> destructiveGlossary = new ArrayList<>();
    @Autowired
    CategoriesRepository categoriesRepository;
    @Autowired
    ContentRepository contentRepository;

    private void init(){
        try {
            File glossaryResource = ResourceUtils.getFile("classpath:destructive_glossary.json");
            JSONArray glossary = new JSONArray( new String(Files.readAllBytes(glossaryResource.toPath())));
            for (Object categoriesObj : glossary) {
                JSONObject categories = (JSONObject) categoriesObj;
                String terms = categories.getString("terms");
                List<String> termsForCategory = tokenizeText(terms);
                Category categoryEntity = new Category();
                categoryEntity.setName(categories.getString("name"));
                categoryEntity.setTerms(termsForCategory.stream().reduce("",(s, s2) -> s + " "+ s2));
                categoriesRepository.save(categoryEntity);
            }
        categoriesRepository.findAll().forEach(destructiveGlossary::add);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void analyze(Document document, Resource resource) {
        if(destructiveGlossary.isEmpty())
            init();
        String text = document.text();
        List<String> tokens = tokenizeText(text);
        List<Category> destructiveCategories = new ArrayList<>();
        for (Category category : destructiveGlossary) {
            List<String> terms = Arrays.asList(category.getTerms().split(" "));
            Integer countOfMatches = 0;
            for (String term : terms) {
                int count = Collections.frequency(tokens, term);
                countOfMatches += count;
            }
            if (countOfMatches > 20)
                destructiveCategories.add(category);
        }
        if(!destructiveCategories.isEmpty()){
            DestructionContent destructionContentByText = contentRepository.findDestructiveContentByResourceUrl(resource.getUrl());
            if(destructionContentByText!=null)
                return;
            DestructionContent content = new DestructionContent();
            content.setText(document.text());
            content.setResource(resource);
            content.setDateFound(new Date());
            content.setCategoryList(destructiveCategories);
            contentRepository.save(content);
        }
    }



    private List<String> tokenizeText(String text) {
        try {
            RussianAnalyzer analyzer = new RussianAnalyzer();
            TokenStream tokenStream = analyzer.tokenStream("", text);
            AttributeImpl attr = tokenStream.getAttributeImplsIterator().next();

            tokenStream.reset();
            List<String> result = new ArrayList<>();
            while (tokenStream.incrementToken()) {
                result.add(attr.toString());
            }

            return result;
        } catch (IOException e) {
            return null;
        }
    }
}
