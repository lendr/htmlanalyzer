package edu.vgtu.analyzer.services;

import edu.vgtu.analyzer.entities.Resource;
import edu.vgtu.analyzer.repositories.ContentRepository;
import edu.vgtu.analyzer.repositories.ResourceRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class HtmlParserService {
    @Autowired
    ResourceRepository resourceRepository;

    @Autowired
    ContentRepository contentRepository;

    @Autowired
    OrderDefineService orderDefineService;

    @Autowired
    DestructiveAnalyzer destructiveAnalyzer;

    public String parse(List<String> urlStrs) {
        List<Resource> orderedResources = orderDefineService.getSortResource(urlStrs);

        for (Resource resource : orderedResources) {
            Document document = getDocument(resource);
            if (document == null) continue;
            Elements linkElements = document.getElementsByTag("a");
            List<String> links = filterLinks(linkElements);
            for (String link : links) {
                Resource innerResource = orderDefineService.saveOrFetchResource(link);
                Document innerDocument = getDocument(innerResource);
                if (innerDocument == null) {
                    continue;
                }
                destructiveAnalyzer.analyze(innerDocument, innerResource);
            }
            destructiveAnalyzer.analyze(document, resource);
        }
        return null;
    }

    private List<String> filterLinks(Elements links) {
        Set<String> result = new HashSet<>();
        for (Element link : links) {
            String url;
            String href = link.attr("href");
            if (href.equals("/") || href.isEmpty()) {
                continue;
            } else if (href.startsWith("http")) {
                url = href;
            } else {
                url = link.baseUri() + href.substring(1);
            }
            result.add(url);
        }
        return result.stream().filter(s -> !s.contains("oauth")).collect(Collectors.toList());
    }

    private Document getDocument(Resource resource) {
        Document document = null;
        if(resource.getUrl().split("//")[1].startsWith("t.me")){//telegramm
            return null;
        }
        try {
            document = Jsoup.connect(resource.getUrl())
                    .userAgent("Chrome/4.0.249.0 Safari/532.5")
                    .referrer("http://www.google.com")
                    .timeout(5000)
                    .get();
        } catch (IOException e) {
            return null;
        }
        String hashSite = DigestUtils.sha1Hex(document.text());
        if (resource.getHash() != null && resource.getHash().equals(hashSite)) {
            return null;
        }
        resource.setHash(hashSite);
        resourceRepository.save(resource);
        return document;
    }


}
