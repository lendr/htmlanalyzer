package edu.vgtu.analyzer.services;

import edu.vgtu.analyzer.entities.Resource;
import edu.vgtu.analyzer.repositories.ResourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class OrderDefineService {
    @Autowired
    ResourceRepository resourceRepository;

    public List<Resource> getSortResource(List<String> urlStrs) {
        List<Resource> result = new ArrayList<>();
        for (String urlStr : urlStrs) {
            Resource resource = saveOrFetchResource(urlStr);
            result.add(resource);
        }
        result.sort(Comparator.comparingInt(Resource::getRank));
        return result;
    }

    public Resource saveOrFetchResource(String urlStr) {
        Resource resource = resourceRepository.findByUrl(urlStr);
        if(resource!=null) {
            return resource;
        }
        else {
            Resource newResource = new Resource();
            newResource.setUrl(urlStr);
            newResource.setRank(123);
            newResource = resourceRepository.save(newResource);
            return newResource;
        }
    }
}
