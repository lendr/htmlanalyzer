package edu.vgtu.analyzer.services;

import edu.vgtu.analyzer.constants.HighChartsConstants;
import edu.vgtu.analyzer.dto.ContentsDTO;
import edu.vgtu.analyzer.entities.Category;
import edu.vgtu.analyzer.entities.DestructionContent;
import edu.vgtu.analyzer.repositories.CategoriesRepository;
import edu.vgtu.analyzer.repositories.ContentRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static edu.vgtu.analyzer.constants.HighChartsConstants.contentsByTime;
import static edu.vgtu.analyzer.constants.HighChartsConstants.pieCategory;

@Service
public class StatisticService {
    @Autowired
    ContentRepository contentRepository;
    @Autowired
    CategoriesRepository categoriesRepository;

    public List<ContentsDTO> getFoundContents() {
        List<ContentsDTO> result = new ArrayList<>();
        contentRepository.findAll().forEach(destructionContent -> {
            ContentsDTO contentsDTO = new ContentsDTO();
            contentsDTO.setFoundDate(destructionContent.getDateFound());
            contentsDTO.setUrl(destructionContent.getResource().getUrl());
            contentsDTO.setCategories(destructionContent.getCategoryList()
                    .stream()
                    .map(Category::getName)
                    .reduce("", (s, categories) -> s + ", " + categories)
                    .replaceFirst(", ", ""));
            result.add(contentsDTO);
        });
        return result;
    }

    public JSONObject getHighChartsByTimeFound() {
        String result = pieCategory;
        Iterable<DestructionContent> allContents = contentRepository.findAll();
        allContents.forEach(destructionContent -> {
            List<String> categories = destructionContent.getCategoryList()
                    .stream()
                    .map(Category::getName)
                    .collect(Collectors.toList());


        });
        return null;
    }

    public JSONObject getHighChartsCategoryByContentSize() {
        JSONObject result = new JSONObject(pieCategory);
        JSONObject title = new JSONObject();
        title.put("text", "Колличество вирусных контентов по категориям");
        result.put("title", title);
        JSONArray data = result.getJSONArray("series").getJSONObject(0).getJSONArray("data");
        Iterator<Category> iterator = categoriesRepository.findAll().iterator();
        while (iterator.hasNext()) {
            Category category = iterator.next();
            JSONObject value = new JSONObject();
            value.put("name", category.getName());
            value.put("y", category.getDestructionContentList().size());
            data.put(value);

        }
        return result;
    }

    public JSONObject getHighChartsContentsByTime() {
        JSONObject result = new JSONObject(contentsByTime);
        JSONObject title = new JSONObject();
        title.put("text", "Статистика появления вирусных контентов");
        result.put("title", title);
        JSONArray data = result.getJSONArray("series").getJSONObject(0).getJSONArray("data");
        Integer[] times = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        Iterator<DestructionContent> iterator = contentRepository.findAll().iterator();
        while (iterator.hasNext()) {
            DestructionContent content = iterator.next();
            int hours = content.getDateFound().getHours();
            if (hours < 2)
                times[0]++;
            else if (hours < 4)
                times[1]++;
            else if (hours < 6)
                times[2]++;
            else if (hours < 8)
                times[3]++;
            else if (hours < 10)
                times[4]++;
            else if (hours < 12)
                times[5]++;
            else if (hours < 14)
                times[6]++;
            else if (hours < 16)
                times[7]++;
            else if (hours < 18)
                times[8]++;
            else if (hours < 20)
                times[9]++;
            else if (hours < 22)
                times[10]++;
            else if (hours < 24)
                times[11]++;
        }
        for (Integer time : times) {
            data.put(time);
        }
        return result;
    }
}
